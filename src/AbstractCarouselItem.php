<?php
namespace Dudley\Patterns\Abstracts;

use Dudley\Patterns\Traits\ImageTrait;

/**
 * Class AbstractCarouselItem
 *
 * @package Dudley\Patterns\Pattern\Carousel
 */
abstract class AbstractCarouselItem extends AbstractItem {
	use ImageTrait;

	/**
	 * AbstractCarouselItem constructor.
	 *
	 * @param array  $img           Image data.
	 * @param string $img_size      Size of the image to render.
	 * @param string $parent_action Action name defined on the carousel module's class.
	 */
	public function __construct( $img, $img_size, $parent_action ) {
		$this->img      = $img;
		$this->img_size = $this->set_img_size( $img_size, $parent_action );
	}

	/**
	 * Requirements to render this item.
	 *
	 * @return array
	 */
	public function requirements() {
		return [
			is_array( $this->img ),
		];
	}

	/**
	 * Print the attributes for the Carousel image.
	 *
	 * @param int $index Image number.
	 */
	public function image_attributes( $index ) {
		echo 'style="background-image: url(' . esc_url( $this->get_img_url() ) . ');"';
		$this->img_index_attr( $index );
	}

	/**
	 * Data image index attribute of the Carousel item.
	 *
	 * @param string $index Index number of the image.
	 */
	public function img_index_attr( $index ) {
		echo 'data-image=image-' . esc_attr( $index ) . '" aria-label="Image ' . esc_attr( $index ) . '"';
	}
}
