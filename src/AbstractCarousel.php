<?php
namespace Dudley\Patterns\Abstracts;

/**
 * Class AbstractCarousel
 *
 * @package Dudley\Patterns\Pattern\Carousel
 */
abstract class AbstractCarousel extends AbstractRepeater {
	/**
	 * Whether the carousel should play automatically.
	 *
	 * @var bool $autoplay
	 */
	private $autoplay;

	/**
	 * Number of seconds between transitions.
	 *
	 * @var int
	 */
	private $autoplay_speed;

	/**
	 * Whether to display jump navigation for the carousel.
	 *
	 * @var bool $jump_nav
	 */
	private $jump_nav;

	/**
	 * AbstractCarousel constructor.
	 *
	 * @param bool $autoplay       Whether to autoplay the carousel.
	 * @param int  $autoplay_speed Speed of the slideshow.
	 * @param bool $jump_nav       Whether to display the jump navigation.
	 */
	public function __construct( $autoplay, $autoplay_speed, $jump_nav ) {
		$this->autoplay       = $autoplay;
		$this->autoplay_speed = $autoplay_speed;
		$this->jump_nav       = $jump_nav;
	}

	/**
	 * Requirements to render the carousel.
	 *
	 * @return array
	 */
	public function requirements() {
		$req = [
			$this->items,
		];

		if ( $this->autoplay ) {
			array_push( $req, $this->autoplay_speed );
		}

		return $req;
	}

	/**
	 * Attribute for the autoplay speed.
	 */
	public function autoplay_speed_attr() {
		echo esc_attr( 'data-autoplay-speed=' . $this->autoplay_speed );
	}

	/**
	 * Attribute for the autoplay value.
	 */
	public function autoplay_attr() {
		echo esc_attr( 'data-autoplay=' . ( $this->autoplay ? 'true' : 'false' ) );
	}

	/**
	 * Autoplay getter.
	 *
	 * @return bool
	 */
	public function get_autoplay() {
		return $this->autoplay;
	}

	/**
	 * Indicates whether the image carousel should display controls to jump to a specific image.
	 *
	 * @return bool
	 */
	public function has_jump_nav() {
		return $this->jump_nav;
	}
}
